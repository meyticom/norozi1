from django.db import models

# Create your models here.

class contactus(models.Model):
    name = models.CharField(max_length=30, null=True,blank=True)
    mail = models.EmailField(null=True,blank=True)
    message = models.TextField()
    mobile = models.CharField(max_length=15)
    phone = models.CharField(max_length=15,null=True,blank=True)

    def __str__(self):
        return self.name



class warranty(models.Model):
    serial = models.CharField(max_length=15)
    time_forosh = models.DateTimeField(verbose_name="تاریخ فروش محصول")
    end_warranty= models.DateTimeField(verbose_name="تاریخ اتمام گارانتی")
    detail = models.TextField(verbose_name="توضیحات")

    def __str__(self):
        return self.serial


class News(models.Model):
    subject= models.CharField(max_length=100)
    text = models.TextField()


class Setting (models.Model):
    name= models.CharField(max_length=30)
    mobile = models.CharField(max_length=11,null=True,blank=True)
    phone = models.CharField(max_length=11,null=True, blank=True)
    address = models.TextField()
    title = models.CharField(max_length=100,null=True,blank=True)
    mail= models.EmailField(null=True,blank=True)

    def __str__(self):
        return self.name