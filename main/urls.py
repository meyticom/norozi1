from django.conf.urls import url
from django.conf.urls.static import static
from django.conf import settings
from main.models import contactus
from main.views import home,contact,aboutus


urlpatterns = [

      url(r'^$', home, name='main'),
      url(r'^contact/$', contact, name='contact'),
      url(r'^aboutus/$', aboutus, name='aboutus')

              ]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)